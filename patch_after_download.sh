#/bin/sh

# Выполнить после начала сборки buildroot и скачивания всех архивов

# обход ошибки: ubsan.c:1473:23: error: ISO C++ forbids comparison between pointer and integer [-fpermissive]
#      || xloc.file == '\0' || xloc.file[0] == '\xff'

sed -i 's|STRICT_WARN = -Wmissing-format-attribute -Woverloaded-virtual -pedantic -Wno-long-long -Wno-variadic-macros -Wno-overlength-strings|STRICT_WARN = -Wmissing-format-attribute -Woverloaded-virtual -pedantic -Wno-long-long -Wno-variadic-macros -Wno-overlength-strings -fpermissive|' misc/buildroot/toolchain_build_arm_nofpu/gcc-6.1.0-build/gcc/Makefile

# обход ошибки двойного декларирования в kconfig-frontends
#sed -i '/kconf_id_lookup/d' misc/buildroot/toolchain_build_arm_nofpu/kconfig-frontends-3.12.0.0/libs/parser/hconf.gperf
#Вот это толко доя версии которая в тулсах, билдрутовая как-то косячно собирается и не нужна!

