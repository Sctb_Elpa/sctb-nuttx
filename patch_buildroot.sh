#/bin/sh

# Выполнить 1 раз после скачивания репозитория.
# настраивает buildroot на сборку gcc dthcbb 6.1.0

# copy buildroot config
config=misc/buildroot/.config
cp misc/buildroot/configs/cortexm3-eabi-defconfig-4.8.5 $config
sed -i 's/BR2_GCC_VERSION_4_8_5=y/BR2_GCC_VERSION_6_1_0=y/' $config
sed -i 's/BR2_GCC_VERSION="4.8.5"/BR2_GCC_VERSION="6.1.0"/' $config

echo "## buildroot prepared"
